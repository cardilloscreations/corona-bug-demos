-- Copyright (C) 2019 Ray Cardillo dba Cardillo's Creations. All Rights Reserved.

--[[
BUG DESCRIPTION:
When running in an iPhone simulator or on device, then Numeric and Decimal text input fields do not show a "done" button on the left, as should be expected when setting the return key using: setReturnKey( key )

SPECIAL NOTES:
To reproduce fully, you must run on device or in a iOS simulator (not Corona Simulator) and do not use the computer keyboard. Without access to a computer keyboard, when running on an actual device, the user has no way to dismiss the input entry because there is no "done" button on the lower left as would normally be expected.

WHEN USING iOS SIMULATOR:
Simulator -> Hardware -> Keyboard -> Connect Hardware Keyboard

NOTICE:
The "numeric" and "decimal" do not have a "go" button on the lower left as would normally be expected.
--]]

--[[
CODE NOTES:
Must run on deivce to see incorrect behavior. Added other types to demonstrate what it's supposed to look like.
--]]

local native = require( "native" )

local INPUT_WIDTH = 100
local INPUT_HEIGHT = 60

local MIN_VALUE = 01
local MAX_VALUE = 99

local textInput1, textInputGroup1

local function onScreenTap( event )
  print("screen tap")
  native.setKeyboardFocus(nil)
end

Runtime:addEventListener("tap", onScreenTap)

local function textInputListener( event )
  if event.phase == "submitted" then
    -- give away focus so we can just process the "ended" event
    native.setKeyboardFocus(nil)
  end

  if event.phase == "ended" then
    local text = event.target.text
    if text == nil or #text == 0 then
      return false
    end

    local value = tonumber(text)

    if value < MIN_VALUE or value > MAX_VALUE then
      print( "Value rejected: "..value )
      native.showAlert(
        "Out of range",
        "VALUE must be between "..MIN_VALUE.." and "..MAX_VALUE..".",
        {"OK"},
        function()
          native.setKeyboardFocus( timeTextInput )
          event.target:setSelection(0,100)
        end )
    else
      print( "Value accepted: "..value )
    end
  end

  return true
end

--
-- "number" input type
--
textInput1 = native.newTextField(
  display.contentCenterX,
  (display.contentHeight * 0.25) - (INPUT_HEIGHT * 0.50),
  INPUT_WIDTH,
  INPUT_HEIGHT
)
textInput1.align = "center"
textInput1.inputType = "number"
textInput1.autocorrectionType = "UITextAutocorrectionTypeNo"
textInput1.spellCheckingType = "UITextSpellCheckingTypeNo"
textInput1:setReturnKey( "go" )
textInput1:addEventListener( "userInput", textInputListener )
textInput1.text = "11"

--
-- "decimal" input type
--
textInput2 = native.newTextField(
  display.contentCenterX,
  (display.contentHeight * 0.50) - (INPUT_HEIGHT * 0.50),
  INPUT_WIDTH,
  INPUT_HEIGHT
)
textInput2.align = "center"
textInput2.inputType = "decimal"
textInput2.autocorrectionType = "UITextAutocorrectionTypeNo"
textInput2.spellCheckingType = "UITextSpellCheckingTypeNo"
textInput2:setReturnKey( "go" )
textInput2:addEventListener( "userInput", textInputListener )
textInput2.text = "22"

--
-- "default" input type
--
textInput3 = native.newTextField(
  display.contentCenterX,
  (display.contentHeight * 0.75) - (INPUT_HEIGHT * 0.50),
  INPUT_WIDTH,
  INPUT_HEIGHT
)
textInput3.align = "center"
textInput3.inputType = "default"
textInput3.autocorrectionType = "UITextAutocorrectionTypeNo"
textInput3.spellCheckingType = "UITextSpellCheckingTypeNo"
textInput3:setReturnKey( "go" )
textInput3:addEventListener( "userInput", textInputListener )
textInput3.text = "33"

