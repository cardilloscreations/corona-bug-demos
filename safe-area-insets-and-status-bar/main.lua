-- Copyright (C) 2019 Ray Cardillo dba Cardillo's Creations. All Rights Reserved.

--[[
BUG DESCRIPTION:
I originally thought there was a bug with the safe area insets in landscape mode, but it turns out that UIStatusBarHidden is not working on iOS and we have to call display.setStatusBar() specifically or else the status bar is not hidden and the safe area insets are not as expected.

DEVICES / SIMULATORS:
iPhone 6, etc, iPad Mini, etc
--]]

--[[
CODE NOTES:
I just put this together very quickly... didn't want to spend a bunch of time making it more dynamic to show landscape vs portrait, etc, and redraw, but the main problem seems to just be that the UIStatusBarHidden is not working (at least in simulator).

See WORKAROUND below, which may be the better approach now that other devices "may" have a status bar in the future. However, it's still a bug for iOS because the simulator (and device) should not show the status bar on iOS if it's configured via UIStatusBarHidden. 
--]]

-- WORKAROUND: comment this in/out and see that this fixes the problem
--display.setStatusBar( display.HiddenStatusBar )

-- use upper left anchors
display.setDefault("anchorX", 0)
display.setDefault("anchorY", 0)

-- show the viewable area in BLUE
local viewableArea = display.newRect(
    0,
    0,
    display.viewableContentWidth,
    display.viewableContentHeight
    )
viewableArea:setFillColor(0, 0, 127)
 
print(string.format("Safe Area Properties:\n safeActualContentWidth = %d\n safeActualContentHeight = %d", display.safeActualContentWidth, display.safeActualContentHeight))

local topInset, leftInset, bottomInset, rightInset = display.getSafeAreaInsets()

-- NOTE: "%02.4f" is not zero padding like sprintf() does
local debugInsetsText = string.format("Safe Area Insets:\n top = %02.4f\n left = %02.4f\n bottom = %02.4f\n right = %02.4f", topInset, leftInset, bottomInset, rightInset)

print(debugInsetsText)

-- show the safe area in RED
local safeArea = display.newRect(
  display.safeScreenOriginX,
  display.safeScreenOriginY,
  display.safeActualContentWidth,
  display.safeActualContentHeight
  )
safeArea:setFillColor(127, 0, 0)

local debugText = display.newText( {
  text = debugInsetsText,
  align = "right",
  fontSize = 36
  } )
debugText.anchorX = 0.5
debugText.anchorY = 0.5
debugText.x = safeArea.x + (safeArea.width*0.5)
debugText.y = safeArea.y + (safeArea.height*0.5)
